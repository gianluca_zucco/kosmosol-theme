<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project parmashop
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

//Teaser switch
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'css_width', array(
    'type'  =>  'text',
    'label' =>  'CSS width',
    'input' =>  'text',
    'required' => false,
    'group' =>  'Megamenu',
    'default' => '700'
));
