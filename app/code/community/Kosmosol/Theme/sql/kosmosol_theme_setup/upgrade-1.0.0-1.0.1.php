<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project parmashop
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

//WTF
$installer->removeAttribute(Mage_Catalog_Model_Category::ENTITY, 'mob2_cat_iphone_cms_block');

//Teaser switch
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'use_teaser', array(
    'type'  =>  'int',
    'label' =>  'Use teaser',
    'input' =>  'select',
    'group' =>  'Megamenu',
    'source' => 'eav/entity_attribute_source_boolean',
    'required' => false
));

//Teaser selector
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'teaser_block', array(
    'type'  =>  'int',
    'label' =>  'Menu block',
    'input' =>  'select',
    'group' =>  'Megamenu',
    'source' => 'catalog/category_attribute_source_page',
    'required' => false,
));

//Menu layout
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'items_layout', array(
    'type'  =>  'int',
    'label' =>  'Layout item',
    'input' =>  'select',
    'group' =>  'Megamenu',
    'required' => false,
    'source' => 'kosmosol_theme/source_layout',
    'default' => '6'
));
