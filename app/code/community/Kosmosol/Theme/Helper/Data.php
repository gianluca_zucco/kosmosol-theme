<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project Polo
 */

class Kosmosol_Theme_Helper_Data extends Mage_Core_Helper_Abstract
{
    const BOOTSTRAP_COLUMNS = 12;

    public function getCartUrl()
    {
        return $this->_getUrl('checkout/cart');
    }

    /**
     * @param $targetHtmlId
     * @return mixed
     */
    public function getMediaManagerDialog($targetHtmlId)
    {
        return Mage::helper('adminhtml')->getUrl('*/cms_wysiwyg_images/index', array(
            'target_element_id' => $targetHtmlId
        ));
    }

    /**
     * Returns calculated block layout
     *
     * @param Mage_Catalog_Model_Category $category
     * @return int
     */
    public function getBlockLayout(Mage_Catalog_Model_Category $category)
    {
        return self::BOOTSTRAP_COLUMNS - $this->getItemLayout($category);
    }

    /**
     * Returns choosen item layout
     *
     * @param Mage_Catalog_Model_Category $category
     * @return int|mixed
     */
    public function getItemLayout(Mage_Catalog_Model_Category $category)
    {
        $layout = $category->getData('items_layout');
        return $layout ? $layout : 6;
    }

    /**
     * Returns static block html
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getMenuTeaser(Mage_Catalog_Model_Category $category)
    {
        $teaserId = $category->getData('teaser_block');
        return $teaserId ? Mage::getSingleton('core/layout')->createBlock('cms/block')->setBlockId($teaserId)->toHtml() : '';
    }


}
