<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project Polo
 */

class Kosmosol_Theme_Block_Checkout_Cart_Link extends Mage_Core_Block_Template
{

    /**
     * Returns currently cart visible items
     *
     * @return array
     */
    public function getCartItems()
    {
        return Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
    }

    /**
     * Returns formatted price
     * @param Mage_Sales_Model_Quote_Item $item
     * @return mixed
     */
    public function getPriceHtml(Mage_Sales_Model_Quote_Item $item)
    {
        return Mage::helper('core')->currency($item->getPrice());
    }

    public function getCartText()
    {
        $count = Mage::helper('checkout/cart')->getItemsCount();
        return $count ? $this->__('My Cart (%s)', $count) : $this->__('My Cart');
    }
}
