<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project Polo
 */

class Kosmosol_Theme_Block_Checkout_Cart_Response extends Mage_Core_Block_Template
{
    /**
     * @return Mage_Catalog_Model_Product
     * @throws Exception
     */
    public function getAddedProduct()
    {
        return Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product', 0));
    }
}
