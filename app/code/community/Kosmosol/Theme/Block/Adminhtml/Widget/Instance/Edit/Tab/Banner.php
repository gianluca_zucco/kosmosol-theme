<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project test
 */

class Kosmosol_Theme_Block_Adminhtml_Widget_Instance_Edit_Tab_Banner extends Mage_Adminhtml_Block_Widget_Form
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('banner_fieldset', array(
            'legend' => $this->__('Banner options')
        ));
        $mediaShow = Mage::getSingleton('core/layout')
            ->createBlock('core/template')
            ->setTemplate('kosmosol/theme/widget/tab/banner.phtml');

        $fieldset->addField('banner_link', 'text', array(
            'name' => 'parameters[banner_link]',
            'label' => 'Link',
            'required' => true,
            'value' => $this->getSelectedCmsLink()
        ));

        $fieldset->addField('banner', 'hidden', array(
            'name' => 'parameters[banner]',
            'label' => 'Banner',
            'value' => $this->getSelectedImageUrl(),
            'after_element_html' => $mediaShow->toHtml()
        ));

        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getSelectedImageUrl() {
        $widget = $this->getWidgetInstance();
        $values = $widget->getWidgetParameters();
        return array_key_exists('banner', $values) ? $values['banner'] : '';
    }

    /**
     * @return Mage_Widget_Model_Widget_Instance
     * @throws Exception
     */
    public function getWidgetInstance()
    {
        if (Mage::registry('current_widget_instance') instanceof Mage_Widget_Model_Widget_Instance) {
            return Mage::registry('current_widget_instance');
        }
        return Mage::getModel('widget/widget_instance')->load($this->getRequest()->getParam('widget'));
    }

    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Banner');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    public function getSelectedCmsLink()
    {
        $widget = $this->getWidgetInstance();
        $values = $widget->getWidgetParameters();
        return array_key_exists('banner_link', $values) ? $values['banner_link'] : '';
    }
}
