<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project drykorn
 */

class Kosmosol_Theme_Block_Adminhtml_Widget_Instance_Edit_Tab_Product_Grid_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        try {
            return sprintf('<img src="%s" width="200px"/>', Mage::helper('catalog/image')->init($row, 'small_image'));
        } catch (Exception $e)  {
            return $e->getMessage();
        }
    }
}
