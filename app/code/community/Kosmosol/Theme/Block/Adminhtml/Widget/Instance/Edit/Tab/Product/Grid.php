<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project drykorn
 */

class Kosmosol_Theme_Block_Adminhtml_Widget_Instance_Edit_Tab_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setId('product_section');
        $this->setUseAjax(true);
        $this->setDefaultFilter(array('in_products' => 1));
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        Mage::register('product_grid_block', $this);
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_products', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'in_outfits',
            'values'            => $this->getSelectedProducts(),
            'align'             => 'center',
            'index'             => 'entity_id'
        ));

        $this->addColumn('name',
            array(
                'header'=> Mage::helper('kosmosol_theme')->__('Name'),
                'index' => 'name',
            ));

        $this->addColumn('sku',
            array(
                'header'=> Mage::helper('kosmosol_theme')->__('SKU'),
                'index' => 'sku'
            ));

        $this->addColumn('image',
            array(
                'header'=> Mage::helper('kosmosol_theme')->__('Image'),
                'index' => 'image',
                'renderer' => 'kosmosol_theme/adminhtml_widget_instance_edit_tab_product_grid_renderer_image'
            ));
        return parent::_prepareColumns();
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_products') {
            $ids = $this->getSelectedProducts();
            if (empty($ids)) {
                $ids = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$ids));
            } else {
                if($ids) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$ids));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes());

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Get children of specified item
     *
     * @param Varien_Object $item
     * @return array
     */
    public function getMultipleRows($item)
    {
        $children = parent::getMultipleRows($item);
        return is_array($children) ? $children : array();
    }

    /**
     * Grid url getter
     *
     * @deprecated after 1.3.2.3 Use getAbsoluteGridUrl() method instead
     * Untrue, it's still used in default template
     *
     * @return string current grid url
     */
    public function getGridUrl()
    {
        return Mage::helper('adminhtml')->getUrl('*/kosmosol_theme_widget_product/ajaxGrid', array(
            '_current' => true,
            'widget' => $this->getRequest()->getParam('widget')
        ));
    }

    /**
     * Return row url for js event handlers
     *
     * @param Mage_Catalog_Model_Product|Varien_Object
     * @return string
     */
    public function getRowUrl($item)
    {
        return '';
    }

    /**
     * @return Mage_Widget_Model_Widget_Instance
     * @throws Exception
     */
    public function getWidgetInstance()
    {
        if (Mage::registry('current_widget_instance') instanceof Mage_Widget_Model_Widget_Instance) {
            return Mage::registry('current_widget_instance');
        }
        return Mage::getModel('widget/widget_instance')->load($this->getRequest()->getParam('widget'));
    }

    public function getSelectedProducts()
    {
        $values = $this->getWidgetInstance()->getWidgetParameters();
        if (isset($values['products'])) {
            return Mage::helper('adminhtml/js')->decodeGridSerializedInput($values['products']);
        }
        return $this->getRequest()->getParam('products', array());
    }

    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Products');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
