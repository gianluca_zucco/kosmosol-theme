<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project drykorn
 */

class Kosmosol_Theme_Block_Adminhtml_Widget_Instance_Edit_Tab_Product_Grid_Serializer extends Mage_Adminhtml_Block_Widget_Grid_Serializer
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _beforeToHtml()
    {
        $this->initSerializerBlock(Mage::registry('product_grid_block'), 'getSelectedProducts', 'parameters[products]', 'products');
    }


    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Serializer');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return true;
    }
}
