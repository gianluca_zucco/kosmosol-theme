<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project Polo
 */

class Kosmosol_Theme_Block_Megamenu extends Mage_Core_Block_Template
{

    /**
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    public function getMainCategories()
    {
        return Mage::getResourceModel('catalog/category_collection')
            ->addNameToResult()
            ->joinUrlRewrite()
            ->addIsActiveFilter()
            ->addAttributeToFilter('include_in_menu', true)
            ->addFieldToFilter('parent_id', Mage::app()->getStore()->getRootCategoryId())
            ->addAttributeToSelect($this->_getAttributesToSelect())
            ->setOrder('position', 'ASC');
    }

    /**
     * @param Mage_Catalog_Model_Category $parent
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    public function getChildren(Mage_Catalog_Model_Category $parent)
    {
        return Mage::getResourceModel('catalog/category_collection')
            ->addNameToResult()
            ->joinUrlRewrite()
            ->addIsActiveFilter()
            ->addAttributeToFilter('include_in_menu', true)
            ->addFieldToFilter('parent_id', $parent->getId())
            ->setOrder('position', 'ASC');
    }

    /**
     * @param Mage_Catalog_Model_Category $category
     * @return int
     */
    public function getCssWidth(Mage_Catalog_Model_Category $category)
    {
        return $category->getData('css_width') ? $category->getData('css_width') : 700;
    }

    /**
     * @return array
     */
    protected function _getAttributesToSelect()
    {
        return array('items_layout', 'teaser_block', 'use_teaser', 'css_width');
    }

}
