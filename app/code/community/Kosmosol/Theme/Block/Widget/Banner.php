<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project test
 */

class Kosmosol_Theme_Block_Widget_Banner extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('kosmosol/theme/widget/banner.phtml');
        $this->setUseContainer(true);
    }

    /**
     * @return string
     */
    public function getGridClass() {
        $cols = $this->getGrid() ? $this->getGrid() : 12;
        return 'col-sm-' . $cols;
    }

    public function getBanner() {
        $bannerPieces = explode('media', $this->getData('banner'));
        $banner = sprintf('%s/%s',
            rtrim(Mage::app()->getStore()->getBaseUrl('media'), '/'),
            ltrim(end($bannerPieces), '/')
        );
        return $banner;
    }
}
