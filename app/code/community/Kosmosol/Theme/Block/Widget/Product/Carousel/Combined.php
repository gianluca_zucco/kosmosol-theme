<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project test
 */

class Kosmosol_Theme_Block_Widget_Product_Carousel_Combined extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    public function getIsHighlightCssClass()
    {
        return $this->getIsHighlight() ? ' highlight ' : '';
    }

    protected function _beforeToHtml()
    {
        $this->_setProductCarouselChild();
        $this->_setBannerChild($this->getBannerPosition());
        return parent::_beforeToHtml();
    }

    /**
     * @return mixed
     */
    public function getProductCarouselGrid()
    {
        $value = explode('+', $this->getGrid());
        return reset($value);
    }

    public function getBannerGrid()
    {
        $value = explode('+', $this->getGrid());
        return end($value);
    }

    protected function _setProductCarouselChild()
    {
        $alias = $this->getBannerPosition() == 'right' ? 'left' : 'right';
        $block = Mage::getSingleton('core/layout')
            ->createBlock('kosmosol_theme/widget_product_carousel');
        $block->addData($this->getData());
        $block->setGrid($this->getProductCarouselGrid());
        $block->setTemplate('kosmosol/theme/widget/product/carousel.phtml');
        $this->setChild($alias, $block);
    }

    protected function _setBannerChild($alias = 'right')
    {
        $block = Mage::getSingleton('core/layout')
            ->createBlock('kosmosol_theme/widget_banner');
        $block->addData($this->getData());
        $block->setGrid($this->getBannerGrid());
        $block->setUseContainer(false);
        $block->setTemplate('kosmosol/theme/widget/banner.phtml');
        $this->setChild($alias, $block);
    }
}
