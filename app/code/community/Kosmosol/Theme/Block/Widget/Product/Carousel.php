<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project drykorn
 *
 * @method int getGrid() returns configured grid width
 * @method int|null getLimit()
 * @method int getIsRandom()
 */

class Kosmosol_Theme_Block_Widget_Product_Carousel extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    const PRODUCT_PRICE_TEMPLATE = 'kosmosol/theme/widget/product/price.phtml';
    protected $_defaultQtyPerBreakPoints = array(
        'lg' => 12,
        'md' => 6,
        'sm' => 2,
        'xs' => 1
    );

    public function showCarouselHeader()
    {
        return $this->getTitle();
    }

    public function showCarouselControl($breakpoint)
    {
        return $this->getProductsCollection()->count() > $this->getQtyPerBreakPoint($breakpoint);
    }

    /**
     * @param null $breakpoint
     * @return array|int
     */
    public function getQtyPerBreakPoint($breakpoint = null)
    {
        $values = array();
        foreach ($this->getBreakPoints() as $_breakpoint)    {
            $_config = $this->getData('item_block_qty_' . $_breakpoint);
            $values[$_breakpoint] = !$_config ? $this->_defaultQtyPerBreakPoints[$_breakpoint] : $_config;
        }
        if ($breakpoint && in_array($breakpoint, $this->getBreakPoints()))    {
            return $values[$breakpoint];
        }
        return $values;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $block = $this->getLayout()->createBlock('catalog/product_price')->setTemplate(self::PRODUCT_PRICE_TEMPLATE);
        $this->setChild('price_renderer', $block);
        return $this;
    }

    /**
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    public function getProductsCollection()
    {
        $categoryId = $this->getData('category_id');
        $_collection = Mage::getModel('catalog/category')->load($categoryId)->getProductCollection();
        $_collection->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addFieldToFilter('visibility', array('gteq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG))
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes());
        //Eventually add limit
        if ($this->getLimit() && is_numeric($this->getLimit()))  {
            $_collection->setPage(0, $this->getLimit());
        }

        if ($this->getIsRandom())   {
            $_collection->getSelect()->order(new Zend_Db_Expr('RAND()'));
        }
        return $_collection;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return mixed
     */
    public function getPriceHtml(Mage_Catalog_Model_Product $product)
    {
        return $this->getChild('price_renderer')->setProduct($product)->toHtml();
    }

    /**
     * @return string
     */
    public function getGridClass()
    {
        return sprintf('col-sm-%s', $this->getGrid());
    }

    /**
     * @return string
     */
    public function getItemGridClass()
    {
        return sprintf('col-sm-%s', intval(12 / $this->getItemBlockQty()));
    }

    /**
     * Returns bootstrap codified breakpoints
     *
     * @return array
     */
    public function getBreakPoints()
    {
        return array('lg', 'md', 'sm', 'xs');
    }

    /**
     * @return int|null
     */
    public function getCategoryId()
    {
        if ($this->getData('category_id')) {
            preg_match('/\d+/', $this->getData('category_id'), $matches);
            return $matches[0];
        }
        return null;
    }

    public function getInterval()
    {
        return ($this->getData('interval') && is_numeric($this->getData('interval'))) ? $this->getData('interval') : 'false';
    }
}
