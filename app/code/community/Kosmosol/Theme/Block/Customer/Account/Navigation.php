<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project Polo
 */

class Kosmosol_Theme_Block_Customer_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{
    public function removeLink($name)
    {
        unset($this->_links[$name]);
    }
}
