<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project Polo
 */


/** @var Mage_Core_Model_Store $store */
foreach (Mage::app()->getStores() as $store) {

    $block = Mage::getModel('cms/block')
        ->setStores($store->getId())
        ->setTitle('Slider block')
        ->setContent('<div class="container">
    <div id="home-slider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#home-slider" data-slide-to="0" class="active"></li>
            <li data-target="#home-slider" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="https://placeholdit.imgix.net/~text?txtsize=150&bg=000000&txt=First+slide&w=1920&h=400" alt="alt #1">
                <div class="carousel-caption">
                    <p>This is a caption</p>
                </div>
            </div>
            <div class="item">
                <img src="https://placeholdit.imgix.net/~text?txtsize=150&bg=0000ff&txt=Second+slide&w=1920&h=400" alt="Alt #2">
                <div class="carousel-caption">
                    <p>This is a caption with a <a href="">Link</a></p>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#home-slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#home-slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
')
        ->setIdentifier('slider_content');

    $block->save();
}

