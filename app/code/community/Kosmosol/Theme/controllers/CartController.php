<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project Polo
 */

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';

class Kosmosol_Theme_CartController extends Mage_Checkout_CartController
{
    /**
     * Add special ajax handles
     *
     * @return $this
     */
    public function addActionLayoutHandles()
    {
        parent::addActionLayoutHandles();
        if ($this->getRequest()->isAjax())  {
            $update = $this->getLayout()->getUpdate();
            $update->addHandle($this->getFullActionName() . '_ajax');
        }
        return $this;
    }

    public function addAction()
    {
        parent::addAction();
        $this->loadLayout();
        $this->_getSession()->getMessages(true);
        $this->renderLayout();
    }

    public function updateHeaderBlockAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function _goBack()
    {
        if ($this->getRequest()->isAjax() && $this->getRequest()->getParam('kosmosol_ajax', false))  {
            return $this;
        }
        return parent::_goBack();
    }
}
