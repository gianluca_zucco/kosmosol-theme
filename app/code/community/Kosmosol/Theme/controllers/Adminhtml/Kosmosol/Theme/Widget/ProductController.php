<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project test
 */

class Kosmosol_Theme_Adminhtml_Kosmosol_Theme_Widget_ProductController extends Mage_Adminhtml_Controller_Action
{
    public function ajaxGridAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('products.grid')->setData('products', $this->getRequest()->getParam('products', null));
        $this->renderLayout();
    }
}
