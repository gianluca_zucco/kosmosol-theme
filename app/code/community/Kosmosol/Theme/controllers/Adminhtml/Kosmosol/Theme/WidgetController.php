<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project test
 */

class Kosmosol_Theme_Adminhtml_Kosmosol_Theme_WidgetController extends Mage_Adminhtml_Controller_Action
{
    public function renderAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('root')
            ->addData($this->getRequest()->getParams());
        $this->renderLayout();
    }
}
