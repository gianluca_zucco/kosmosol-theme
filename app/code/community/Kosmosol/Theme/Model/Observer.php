<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project test
 */
class Kosmosol_Theme_Model_Observer
{
    public function addWidgetHandle(Varien_Event_Observer $observer)
    {
        $widget = Mage::registry('current_widget_instance');
        if ($widget instanceof Mage_Widget_Model_Widget_Instance) {
            $blockType = substr($widget->getType(), strpos($widget->getType(), '/')+1);
            if ($blockType) {
                Mage::getSingleton('core/layout')->getUpdate()->addHandle($blockType);
            }
        }
    }

    public function allowStaticImagesUrl(Varien_Event_Observer $observer)
    {
        $observer->getResult()->isAllowed = true;
    }
}
