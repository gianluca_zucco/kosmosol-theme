<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project Polo
 */

class Kosmosol_Theme_Model_Source_Itemperslide
{
    public function toOptionArray()
    {
        return array(
            array(
                'label' => '12',
                'value' => '12'
            ),
            array(
                'label' => '6',
                'value' => '6'
            ),
            array(
                'label' => '4',
                'value' => '4'
            ),
            array(
                'label' => '3',
                'value' => '3'
            ),
            array(
                'label' => '2',
                'value' => '2'
            ),
            array(
                'label' => '1',
                'value' => '1'
            )
        );
    }
}
