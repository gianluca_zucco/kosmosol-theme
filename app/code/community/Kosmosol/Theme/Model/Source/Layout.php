<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project parmashop
 */
class Kosmosol_Theme_Model_Source_Layout extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        return array(
            array(
                'value' =>  '6',
                'label' =>  '1/2'
            ),
            array(
                'value' =>  '4',
                'label' =>  '1/3'
            ),
            array(
                'value' =>  '2',
                'label' =>  '1/6'
            )
        );
    }
}
