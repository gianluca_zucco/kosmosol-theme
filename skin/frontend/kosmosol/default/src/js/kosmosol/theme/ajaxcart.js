jQuery(document).on('ready', function() {
    jQuery('button.btn-cart')
        .each(function() {
            var url = /setLocation\('(.+)'\)/.exec(this.readAttribute('onclick'));
            if (url) {
                this.writeAttribute('data-url', url.pop());
                this.removeAttribute('onclick');
            } else {
                //Product page case
                this.writeAttribute('data-url', $('product_addtocart_form').readAttribute('action'));
                this.removeAttribute('onclick');
            }
        })
        .on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            jQuery.ajax(this.readAttribute('data-url'), {
                method: 'POST',
                data: {
                    is_ajax: true,
                    kosmosol_ajax: true,
                    qty: $('qty') ? $F('qty') : 1
                },
                beforeSend: function() {
                    jQuery('#modal-dialog-container')
                        .html(jQuery('#ajax-loader').html());
                    jQuery('#cart-modal').modal();
                },
                success: function(htmlResponse) {
                    jQuery('#modal-dialog-container')
                        .html(htmlResponse);
                },
                complete: function()    {
                    jQuery(document).trigger('cart:header:reload', {
                        url: updateCartUrl
                    });
                }
            });
        });
});
